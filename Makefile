.DEFAULT_GOAL := help
help:
	@echo "'make full-deployment' 					to build all"
	@echo "'make instance-deployment'  				to build infrastructure and instance"
	@echo "'make clean' 						to destroy infrastrucure and instance"


full-deployment:
	@packer build -var-file=packer/var-file.json -var-file=packer/credential-var.json packer/packer-image-nginx.json
	@cd terraform && terraform apply -var-file terraform.tfvars -auto-approve 
	# @ansible-playbook -i ansible/ec2.py -u ubuntu  --private-key /home/elnemr/mint.pem ansible/playbook.yaml

instance-deployment:
	@cd terraform && terraform apply -var-file terraform.tfvars -auto-approve 
	# @ansible-playbook -i ansible/ec2.py -u ubuntu  --private-key /home/elnemr/mint.pem ansible/playbook.yaml

# instance-deployment-without-config:
# 	@cd terraform && terraform apply -auto-approve

# change-deployment:
# 	@ansible-playbook -i ansible/ec2.py -u ubuntu  --private-key /home/elnemr/mint.pem ansible/playbook.yaml
clean:
	@cd terraform && terraform destroy  -auto-approve
