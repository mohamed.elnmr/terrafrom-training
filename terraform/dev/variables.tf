variable "aws_access_key" {
}
variable "aws_secret_key" {
}
# variable "aws_key_path" {}
# variable "aws_key_name" {}
variable "second_aws_access_key" {
}
variable "second_aws_secret_key" {
}
variable "aws_region" {
    description = "EC2 Region for the VPC"
    default     = "us-west-1"
}