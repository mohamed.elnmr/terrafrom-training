provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region     = "${var.aws_region}"
}

provider "aws" {
    alias      = "route53"
    access_key = "${var.second_aws_access_key}"
    secret_key = "${var.second_aws_secret_key}"
    region     = "${var.aws_region}"
}


module "terraform-infrastructure" {
    
        source   = "../modules/infrastructure"
        enabled  = "true"
}


module "terraform-load-balancer"{
    source               = "../modules/loadbalancer"
    prefix               = "default"
    public-subnet-id     = "${module.terraform-infrastructure.public-subnet-id}"
    vpc-id               = "${module.terraform-infrastructure.vpc-id}"
    attached-instance-id = "${module.ec2-instance.attached-instance-id}"


}
module "ec2-instance" {
  source                 = "../modules/instances"
  public-subnet-id       = "${module.terraform-infrastructure.public-subnet-id}"
  private-subnet-id      = "${module.terraform-infrastructure.private-subnet-id}"
  aws-key-name           = "mint"
  jump-security-group    = "${module.terraform-infrastructure.jump-security-group}"
  nat-security-group     = "${module.terraform-infrastructure.nat-security-group}"
  private-security-group = "${module.terraform-infrastructure.private-security-group}"
  config-script-path     = "../modules/instances/script.sh"
  ansible-path           = "../../ansible"
  elastic-ip             = "${module.terraform-infrastructure.elastic-ip}"


  
}

module "route53" {
  source   = "../modules/route53"
#   provider = aws.route53
  zone-id  = "Z17JJZTLWU8KAL"
  name     = "dev.roadstracking.com"
  record-type     = "CNAME"
  ttl      = "300"
  records  = "${module.ec2-instance.public-dns}"

}


