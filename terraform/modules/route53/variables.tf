variable "provider" {
  default="aws.route53"
}

variable "zone-id" {
  
}


variable "name" {
  type="string"
}

variable "record-type" {
  default="CNAME"
}

variable "ttl" {
  default="300"
}

variable "records" {
  
}




