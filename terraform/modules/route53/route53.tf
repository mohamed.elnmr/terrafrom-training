resource "aws_route53_record" "subdomain" {
  provider = "aws.route53"
  zone_id  = "${var.zone-id}"
  name     = "${var.name}"
  type     = "${var.record-type}"
  ttl      = "${var.ttl}"
  records  = ["${var.records}"]

}