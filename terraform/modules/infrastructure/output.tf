output "vpc-id" {
  value = "${aws_vpc.default.id}"
}

output "public-subnet-id" {
  value = "${aws_subnet.us-west-1a-public.id}"
}

output "private-subnet-id" {
  value = "${aws_subnet.us-west-1a-private.id}"
}


output "availability-zones" {
  value = "${var.availability-zone}"
}

output "private-subent-cidr" {
  value = "${var.private-subnet-cidr}"
}

output "vpc-cidr" {
  value = "${var.vpc-cidr}"
}

output "jump-security-group" {
  value = "${aws_security_group.jump-server.id}"
}

output "nat-security-group" {
  value = "${aws_security_group.nat.id}"
}

output "private-security-group" {
  value = "${aws_security_group.private-server-security-group.id}"
}

output "elastic-ip" {
  value = "${aws_eip.elastic-ip.id}"
}



