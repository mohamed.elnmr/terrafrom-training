resource "aws_vpc" "default" {
    count                = "${var.enabled == "true" ? 1 : 0}"
    cidr_block           = "${var.vpc-cidr}"
    enable_dns_hostnames = true
    tags {
        Name = "${var.prefix}-terrafrom-VPC"
    }
}

resource "aws_internet_gateway" "default" {
    count  = "${var.enabled == "true" ? 1 : 0}"
    vpc_id = "${aws_vpc.default.id}"
    tags {
        Name = "${var.prefix}-terrafrom-internet-gateway"
    }
}

/*ds
  Public Subnet
*/
resource "aws_subnet" "us-west-1a-public" {
    count  = "${var.enabled == "true" ? 1 : 0}"
    vpc_id = "${aws_vpc.default.id}"

    cidr_block        = "${var.public-subnet-cidr}"
    availability_zone = "${var.availability-zone["branch"]}"

    tags {
        Name = "${var.prefix}-public-subnet"
    }
}

resource "aws_route_table" "us-west-1a-public" {
    count  = "${var.enabled == "true" ? 1 : 0}"
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "${var.world-cidr}"
        gateway_id = "${aws_internet_gateway.default.id}"
    }

    tags {
        Name = "${var.prefix}-public-route-table"
    }
}

resource "aws_route_table_association" "us-west-1a-public" {
    count          = "${var.enabled == "true" ? 1 : 0}"
    subnet_id      = "${aws_subnet.us-west-1a-public.id}"
    route_table_id = "${aws_route_table.us-west-1a-public.id}"
}

/*
  Private Subnet
*/
resource "aws_subnet" "us-west-1a-private" {
    count  = "${var.enabled == "true" ? 1 : 0}"
    vpc_id = "${aws_vpc.default.id}"

    cidr_block        = "${var.private-subnet-cidr}"
    availability_zone = "${var.availability-zone["branch"]}"

    tags {
        Name = "${var.prefix}-private-subent"
    }
}

resource "aws_route_table" "us-west-1a-private" {
    count  = "${var.enabled == "true" ? 1 : 0}"
    vpc_id = "${aws_vpc.default.id}"

    route {
          cidr_block  = "${var.world-cidr}"
          nat_gateway_id = "${aws_nat_gateway.nat_gateway.id}"
    }

    tags {
        Name = "${var.prefix}-private-route-table"
    }
}

resource "aws_route_table_association" "us-west-1a-private" {
    count          = "${var.enabled == "true" ? 1 : 0}"
    subnet_id      = "${aws_subnet.us-west-1a-private.id}"
    route_table_id = "${aws_route_table.us-west-1a-private.id}"
}



resource "aws_security_group" "jump-server" {   
    name        = "${var.prefix}-vpc_jump-server"
    description = "Allow ssh connection to private subnet "

    
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

ingress { //only for testing 
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {   //only for testing
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    

        egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["${var.world-cidr}"]
        }
    

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.prefix}-jump-ServerSG"
    }
}

resource "aws_security_group" "nat" {
    name        = "${var.prefix}-vpc_nat"
    description = "Allow traffic to pass from the private subnet to the internet"

    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "tcp"
        cidr_blocks = ["${var.private-subnet-cidr}"]
    }
    
    
    
    

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }


    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.prefix}-nat-SG"
    }
}


resource "aws_security_group" "private-server-security-group" {
      name        = "${var.prefix}-private-server-security-groups"
      description = "Allow ssh connection to private instance and receive traffic from loadbalancer"

      ingress {
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_blocks = ["${var.public-subnet-cidr}"]

      }
      ingress {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["${var.public-subnet-cidr}"]

      }
      ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["${var.public-subnet-cidr}"]
      }
        egress{
          from_port   = 0
          to_port     = 0
          protocol    = "-1"
          cidr_blocks = ["${var.public-subnet-cidr}"]
      }
        vpc_id = "${aws_vpc.default.id}"

      tags {
        Name = "${var.prefix}-private-SG"
    }
}


resource "aws_eip" "elastic-ip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
      allocation_id = "${aws_eip.elastic-ip.id}"
      subnet_id     = "${aws_subnet.us-west-1a-public.id}"
    tags {
        Name = "${var.prefix}-vpc-nat"
    }
}