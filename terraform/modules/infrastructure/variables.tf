

variable "vpc-cidr" {
    description = "CIDR for the whole VPC"
    default     = "10.0.0.0/16"
}

variable "public-subnet-cidr" {
    description = "CIDR for the Public Subnet"
    default     = "10.0.0.0/24"
}

variable "private-subnet-cidr" {
    description = "CIDR for the Private Subnet"
    default     = "10.0.1.0/24"
}

variable "world-cidr" {
    default = "0.0.0.0/0"
  
}


variable "availability-zone" {
    type    = "map"
    default = {
        "main"   = "us-west-1"
        "branch" = "us-west-1a"
    }  
}

variable "boolean" {
    default = true
}

variable "prefix" {
  default = "dev-"
}


variable "enabled" {
  type    = "string"
  default = "true"
}

variable "port" {
  default="80"
  
}





