variable "load_balancer_type" {
    type    = "map"
    default = {
        "network"     = "network"
        "application" = "application"
    }
  
}

variable "prefix" {
  default = "dev-"
}

variable "port" {
  default = "80"
}

variable "protocol" {
  default = "TCP"
}

variable "public-subnet-id" {
}


variable "vpc-id" {
  
}
variable "attached-instance-id" {

  
}

