

resource "aws_lb" "terraform-elb"{
    name               = "${var.prefix}-terraform-load-balancer"
    load_balancer_type = "${var.load_balancer_type["network"]}"
    internal           = false
    tags               = {
        Name = "${var.prefix}-terraform-lb"
        }
    subnets = ["${var.public-subnet-id}"]
}

resource "aws_lb_target_group" "terrafrom-task"{
    name     = "${var.prefix}-terrafrom-task"
    port     = "${var.port}"
    protocol = "${var.protocol}"
    stickiness {
        enabled = false
        type    = "lb_cookie"
    }    
vpc_id = "${var.vpc-id}"
}
resource "aws_lb_listener" "load-balancer-listener" {
    load_balancer_arn = "${aws_lb.terraform-elb.arn}"
    port              = "${var.port}"
    protocol          = "${var.protocol}"
    default_action{
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.terrafrom-task.arn}"
    }
  

}


resource "aws_lb_target_group_attachment" "server-lb" {
  target_group_arn = "${aws_lb_target_group.terrafrom-task.arn}"
  target_id        = "${var.attached-instance-id}"
  port             = "${var.port}"
}



output "dns-name" {
  value = "${aws_lb.terraform-elb.dns_name}"
}
