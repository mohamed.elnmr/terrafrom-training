

resource "aws_instance" "jump-server-1" {
    ami                         = "${var.ami_id["normal-ec2"]}"
    availability_zone           = "${var.availability_zone["branch"]}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.aws-key-name}"
    vpc_security_group_ids      = ["${var.jump-security-group}"]
    subnet_id                   = "${var.public-subnet-id}"
    associate_public_ip_address = true
    source_dest_check           = false


    tags {
        Name = "${var.prefix}-jump-server"
    }
    
    provisioner "file" {
    source      = "${var.config-script-path}"
    destination = "script.sh"
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
  provisioner "file" {
    source      = "/home/elnemr/mint.pem"
    destination = "mint.pem"
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
  provisioner "file" {
    source      = "${var.ansible-path}"
    destination = "/home/ubuntu/ansible/"
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
  provisioner "remote-exec" {
      inline = ["sudo chmod +x /home/ubuntu/script.sh"]
      inline = ["./script.sh"]
      connection{
          type        = "ssh"
          user        = "${var.user}"
          private_key = "${file("${var.private-key-path}")}"
      }

  }
}

data "aws_ami" "packer-ami"{
    owners = ["self"]
   
    filter{
        name   = "tag:Name"
        values = ["packer-ansible"]
    }
}
resource "aws_instance" "packer-nginx" {
    ami                         = "${data.aws_ami.packer-ami.image_id}"
    availability_zone           = "${var.availability_zone["branch"]}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.aws-key-name}"
    vpc_security_group_ids      = ["${var.jump-security-group}"]         // for just testing need to be modified to private
    subnet_id                   = "${var.public-subnet-id}"              // for just testing  need to be modified to private
    associate_public_ip_address = true
    source_dest_check           = false
    private_ip                  = "${var.private_ip}"
    

    tags = {
        Name = "${var.prefix}-packer-nginx"
    }

}


output "attached-instance-id" {
  value = "${aws_instance.packer-nginx.id}"
}


output "public-dns" {
  value = "${aws_instance.packer-nginx.public_dns}"
}
