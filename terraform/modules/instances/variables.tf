variable "ami_id" {
    type    = "map"
    default = {
        "nat"        = "ami-004b0f60"
        "normal-ec2" = "ami-0dbf5ea29a7fc7e05"
    }

  
}
variable "instance_type" {
    default = "t2.micro"
  
}

variable "private_ip" {
    default = "10.0.0.210"
  
}

variable "prefix" {
  default = "dev-"
}

variable "private-key-path" {
  default = "/home/elnemr/mint.pem"
}

variable "user" {
  default = "ubuntu"
}

variable "private-key" {
  default = "mint.pem"
}

variable "availability_zone" {
    type    = "map"
    default = {
        "main"   = "us-west-1"
        "branch" = "us-west-1a"
    }  
}
variable "public-subnet-id" {
  
}

variable "private-subnet-id" {
  
}

variable "aws-key-name" {
  
}

variable "jump-security-group" {
  
}

variable "nat-security-group" {
  
}
variable "private-security-group"{

}


variable "config-script-path" {
  
}

variable "ansible-path" {
  
}

variable "elastic-ip" {
  
}











