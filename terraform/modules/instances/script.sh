#!/bin/bash

sudo echo -e "HOST server \n HostName 10.0.0.210 \nuser ubuntu \nIdentityFile ~/mint.pem \n StrictHostKeyChecking no">>~/.ssh/config
chmod 400 mint.pem
scp -r -i mint.pem  ansible/files server:~/files
sudo apt-add-repository -y ppa:ansible/ansible
sudo add-apt-repository -y universe
sudo add-apt-repository -y ppa:certbot/certbot

sudo apt-get update -y 
sudo apt-get install -y software-properties-common
sudo apt-get  install -y ansible
ansible-playbook -i ansible/hosts ansible/playbook.yaml
sleep 2m 
ansible-playbook -i ansible/hosts ansible/playbook2.yaml

# rm -rf mint.pem
# exit


